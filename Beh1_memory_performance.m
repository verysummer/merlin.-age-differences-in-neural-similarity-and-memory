% Memory performance analysis (proportions high, medium, low memory quality
% items, and age differences)
% Creates Figure 3

% load behavioral data 
load('BehDataMerlinAll.mat')

% subject numbers (last two rows are means and SDs, respectively)
nya = length(YA_rel)-2;
noa = length(OA_rel)-2;

% proportions per memory quality
low.YA = YA_rel(1:nya,2);
med.YA = YA_rel(1:nya,3);
high.YA = YA_rel(1:nya,5);
excl.YA = YA_rel(1:nya,4);

low.OA = OA_rel(1:noa,2);
med.OA = OA_rel(1:noa,3);
high.OA = sum(OA_rel(1:noa,[5,9]),2);
excl.OA = sum(OA_rel(1:noa,[4,6:8]),2);


%% t-tests

[H,P,CI,STATS] = ttest2(high.YA,high.OA)
[H,P,CI,STATS] = ttest2(med.YA,med.OA)
[H,P,CI,STATS] = ttest2(low.YA,low.OA)
[H,P,CI,STATS] = ttest2(excl.YA,excl.OA)

rem.YA = high.YA + med.YA;
rem.OA = high.OA + med.OA;

[H,P,CI,STATS] = ttest2(rem.YA,rem.OA)



%% raincloud plots Figure 4

bandwith = .025;
bo = 1; % box on or off

figure
subplot(4,1,1)
h1 = raincloud_plot_vrs('X', high.YA, 'box_on', bo,'bandwidth', bandwith, 'color', [0.9 0.4 0.5], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .15, 'dot_dodge_amount', .35, 'box_col_match', 1);
h1 = raincloud_plot_vrs('X', high.OA, 'box_on', bo,'bandwidth', bandwith, 'color', [0.6 0.6 0.9], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .55, 'dot_dodge_amount', .75, 'box_col_match', 1);
set(gca,'ytick',[],'FontSize',16,'xlim',[-0.1 1])
ax=gca;
ax.XTickLabelRotation = 270;
subplot(4,1,2)
h1 = raincloud_plot_vrs('X', med.YA, 'box_on', bo,'bandwidth', bandwith, 'color', [0.9 0.4 0.5], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .15, 'dot_dodge_amount', .35, 'box_col_match', 1);
h1 = raincloud_plot_vrs('X', med.OA, 'box_on', bo,'bandwidth', .03, 'color', [0.6 0.6 0.9], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .55, 'dot_dodge_amount', .75, 'box_col_match', 1);
set(gca,'ytick',[],'FontSize',16,'xlim',[-0.1 1])
ax=gca;
ax.XTickLabelRotation = 270;
subplot(4,1,3)
h1 = raincloud_plot_vrs('X', low.YA, 'box_on', bo,'bandwidth', bandwith, 'color', [0.9 0.4 0.5], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .15, 'dot_dodge_amount', .35, 'box_col_match', 1);
h1 = raincloud_plot_vrs('X', low.OA, 'box_on', bo,'bandwidth', bandwith, 'color', [0.6 0.6 0.9], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .55, 'dot_dodge_amount', .75, 'box_col_match', 1);
set(gca,'ytick',[],'FontSize',16,'xlim',[-0.1 1])
ax=gca;
ax.XTickLabelRotation = 270;
subplot(4,1,4)
h1 = raincloud_plot_vrs('X', excl.YA, 'box_on', bo,'bandwidth', .002, 'color', [0.9 0.4 0.5], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .15, 'dot_dodge_amount', .35, 'box_col_match', 1);
h1 = raincloud_plot_vrs('X', excl.OA, 'box_on', bo,'bandwidth', .002, 'color', [0.6 0.6 0.9], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .55, 'dot_dodge_amount', .75, 'box_col_match', 1);
set(gca,'ytick',[],'FontSize',16,'xlim',[-0.01 0.1])
ax=gca;
ax.XTickLabelRotation = 270;
