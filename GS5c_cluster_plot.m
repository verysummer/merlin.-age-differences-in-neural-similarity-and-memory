% Plots cluster masked t maps and topoplots (from GS5a)
% Figure 5C + D

pn.root = '//mpib22/ConMem-EEG/';
addpath('cbrewer'); %colormap designer
addpath('fieldtrip-20150930/');


%% YA
load([pn.root '/MERLIN_ALL/G_RSA/B_stat/YA_half_simwithincond_reduced_noiseremoved_3cond_1000perm.mat'])
stat.statz = cm_t2z(stat.stat,49);
stat = rmfield(stat,'stat'); stat.stat = stat.statz; 

Cm = cbrewer('div', 'RdBu', 128); % choose colormap 
Cm = flipud(Cm); % 

mask = NaN(size(stat.negclusterslabelmat));
mask((find(stat.negclusterslabelmat ==1)))=1;  % first neg cluster
stat.statm = stat.stat.*mask; 

staty = squeeze(nanmean(stat.statm,1));
staty(isnan(staty))=0; % change nans

figure
clims = [-4 4];
i = imagesc(stat.time,stat.freq,staty,clims);
set(gca,'YDir','normal')
colormap(Cm)
c=colorbar
set(gca,'FontSize',18)
set(c,'YTick',[-4:1:4])
ylabel(c, 'z')
set(gca,'FontSize',18)
set(gca, 'XTick', [0:1:3],'YTick', [0:1:3])
xlabel('trial time (s)')
ylabel('trial time (s)')
axis square


% Topoplot

load('realistic_1005.mat')
pcfg = [];
tmp.elec_sel = cm_elec2dataelec_20140226(realistic_1005,stat);
pcfg.elec    = tmp.elec_sel;
%correct some flipping of the elec
pcfg.elec.pnt(:,1) = -tmp.elec_sel.pnt(:,2);
pcfg.elec.pnt(:,2) = tmp.elec_sel.pnt(:,1);%clear tmp;
pcfg.layout = ft_prepare_layout(pcfg); % ERROR IN 2016

%find the relevant electrodes
elecs = find(nanmean(nanmean(mask,3),2)==1);
sign_elecs = stat.label(elecs);

%find the relevant "frequencies" (=time)
freq = find(nanmean(nanmean(mask,1),3)==1);
sign_freq = stat.freq(freq);
minif=min(sign_freq);
maxif=max(sign_freq);

%find the time-window
time = find(nanmean(nanmean(mask,1),2)==1);
sign_time = stat.time(time);
minit=min(sign_time);
maxit=max(sign_time);

pcfg.parameter = 'stat';
pcfg.ylim = [minif maxif]; 
pcfg.xlim = [minit maxit]; 
pcfg.highlight = 'on';
pcfg.highlightchannel = sign_elecs; 
pcfg.highlightsize = 16 ;
pcfg.marker = 'on';
pcfg.markerstyle = '.';
pcfg.markersize = 2;
pcfg.colorbar = 'yes';
pcfg.colormap = Cm;  
pcfg.comment = 'no';
pcfg.zlim = [-3 3];
   
hFigure = figure('Color',[1 1 1]); axes1 = axes('Parent',hFigure,'Layer','top','FontSize',20);
hold(axes1,'all'); ft_topoplotER(pcfg,stat)


%% OA
clear stat
load([pn.root '/MERLIN_ALL/G_RSA/B_stat/OA_half_simwithincond_reduced_noiseremoved_3cond_1000perm.mat'])
stat.statz = cm_t2z(stat.stat,62); 
stat = rmfield(stat,'stat'); stat.stat = stat.statz; 

mask = NaN(size(stat.posclusterslabelmat));
mask((find(stat.posclusterslabelmat ==1)))=1;  % first pos cluster
stat.statm = stat.stat.*mask; 

stato = squeeze(nanmean(stat.statm,1));
stato(isnan(stato))=0; % change nans

figure;
i = imagesc(stat.time,stat.freq,stato,clims);
set(gca,'YDir','normal')
colormap(Cm)
c=colorbar;
set(c,'YTick',[-4:1:4])
%ylabel(c, 't')
set(gca,'FontSize',18)
set(gca, 'XTick', [0:1:3],'YTick', [0:1:3])
xlabel('trial time (s)')
ylabel('trial time (s)')
axis square


% topoplot

%find the relevant electrodes
elecs = find(nanmean(nanmean(mask,3),2)==1);
sign_elecs = stat.label(elecs);

%find the relevant "frequencies" (=time)
freq = find(nanmean(nanmean(mask,1),3)==1);
sign_freq = stat.freq(freq);
minif=min(sign_freq);
maxif=max(sign_freq);

%find the time-window
time = find(nanmean(nanmean(mask,1),2)==1);
sign_time = stat.time(time);
minit=min(sign_time);
maxit=max(sign_time);

pcfg.parameter = 'stat';
pcfg.ylim = [minif maxif]; 
pcfg.xlim = [minit maxit]; 
pcfg.highlight = 'on';
pcfg.highlightchannel = sign_elecs; 
pcfg.highlightsize = 16 ;
pcfg.marker = 'on';
pcfg.markerstyle = '.';
pcfg.markersize = 2;
pcfg.colorbar = 'yes';
pcfg.colormap = Cm;  
pcfg.comment = 'no';
pcfg.zlim = [-3 3];
   
hFigure = figure('Color',[1 1 1]); axes1 = axes('Parent',hFigure,'Layer','top','FontSize',20);
hold(axes1,'all'); ft_topoplotER(pcfg,stat)


