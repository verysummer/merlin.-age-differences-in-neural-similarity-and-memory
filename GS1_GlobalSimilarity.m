function GS1_GlobalSimilarity(id, chan, sample_num)
% Calculates global between-item pattern similarity (Pearson correlation) 
% of time-frequency representations, separately for each subject (id) and
% channel (chan).
%
% Input data is separated according to memory quality (high, medium, low),
% similarity is calculated between items within each memory quality. 
% The number of items per memory quality is reduced based on the memory
% quality category with the fewest items. A random sample of items is
% picked from each category and each item is correlated with every other
% item. This is repeated as often as specified in sample_num. Similarity
% matrices of all samples are averaged and saved.
%
% INPUTS:
% chan :  1-60 (string), e.g. '1'
% id: subject id XXXX (string), e.g. '3101'
% sample_num: 1-N (string), e.g. '20'

% OUTPUT:
% time-time similarity matrices for given subject and channel, and all
% memory conditions (high, medium, low memory quality)

% original VRS 10/2016
% recent version 09/2018
% simplified for publication 06/2019

%%
id = str2num(id);
chan = str2num(chan);
sample_num = str2num(sample_num);

Rem = {'11','01','00'};     % high, medium, low memory quality

% path definitions
pn.root = '/home/mpib/vsommer/globalsim/';  % pn.root = 'G:/ConMem/EEG/';     % citrix

addpath(genpath([pn.root 'MERLIN/S_Scripts/spectral_rsa']))

% EEG tools folder
pn.fun = [pn.root 'ConMemEEGTools/'];
addpath([pn.fun 'fieldtrip-20161020'])

% general path
pn.gen = [pn.root 'MERLIN/D_data/'];

if id > 4000
    actualgroup = 'OA'; % old adults
    actualcond = 'L1'; % first learning phase eeg data

elseif id < 4000
    actualgroup = 'YA'; % young adults
    actualcond = 'L1'; %first learning phase eeg data
end;

actualsubj = id;

pn.eeg = [pn.gen num2str(actualsubj) '/eeg/'];


%% read eeg data (all memory qualities)
fprintf('loading data of subject %d \n', actualsubj);
tic
for r = 1:size(Rem,2)
    load([pn.eeg num2str(actualsubj) '_' actualcond '_' 'tfr_' Rem{r} '_2_100Hz_ST.mat']) % data
end
toc

% assess number of stimuli
num_high = size(tfrdat11.stim,1);
num_med = size(tfrdat01.stim,1);
num_low = size(tfrdat00.stim,1);

% repeat RSA as often as determined by sample_index
for sample_index = 1:sample_num
    tic
    
    % reduce stimulus number to smallest condition
    mini = min([num_high, num_med, num_low]); % smallest cond  

    rng('shuffle') % seeds the random number generator based on the current time
    % select which items to keep from "larger" conditions (random)
    high = randperm(num_high,mini);
    medium = randperm(num_med,mini);
    low = randperm(num_low,mini);

    % delete all others
    tfrdat11.stim = tfrdat11.stim(high,:,:);
    tfrdat11.powspctrm = tfrdat11.powspctrm(high,:,:,:);

    tfrdat01.stim = tfrdat01.stim(medium,:,:);
    tfrdat01.powspctrm = tfrdat01.powspctrm(medium,:,:,:);

    tfrdat00.stim = tfrdat00.stim(low,:,:);
    tfrdat00.powspctrm = tfrdat00.powspctrm(low,:,:,:);

    % new number of stim:
    num_high = mini;
    num_med = mini;
    num_low = mini;    

    num_stims = num_high + num_med + num_low;

    % concatenate separate datasets for irem,frem,nrem to one dataset
    Data = struct;
    Data.label = tfrdat00.label;     % same for high,med,low
    Data.dimord = tfrdat00.dimord;   % "
    Data.freq = tfrdat00.freq;       % "
    Data.time = tfrdat00.time;       % "
    Data.time(1:31) = [];            % delete first 31 time points because NaNs 
    Data.elec = tfrdat00.elec;       % "

    Data.stim = cat(1,tfrdat11.stim,tfrdat01.stim,tfrdat00.stim); % concatenate stimuli
    Data.stim(1:num_high, 4) = cellstr('high');    % add 4th column with rem information
    Data.stim(num_high+1 : num_high+num_med, 4) = cellstr('medium');
    Data.stim(num_high+num_med+1 : length(Data.stim), 4) = cellstr('low');   


    %% Calculate Global Similarity

    % concatenate powerspectra
    Data.powspctrm = cat(1,tfrdat11.powspctrm,tfrdat01.powspctrm,tfrdat00.powspctrm);
    Data.powspctrm(:,:,:,1:31) = [];    % delete data of first 31 time points because NaNs size

    % remove noise spectrum (BOSC)
    cfg           = [];
    cfg.parameter = 'powspctrm';
    cfg.operation = 'log10';

    addpath(genpath([pn.root 'MERLIN/S_Scripts/spectral_rsa']))
    warning('off','stats:statrobustfit:IterationLimit')
    Data_c = mwb_subtract_mean_noise_spectrum(cfg,Data);
    clear Data;

    %log-transform power 
    Data_logpow    = ft_math(cfg, Data_c);
    Data_logpow.stim = Data_c.stim;
    clear Data_c;

    % Preallocations
    meanperstim_high = zeros(num_high,length(Data_logpow.time), length(Data_logpow.time)); % #high x 470x470
    meanperstim_med = zeros(num_med,length(Data_logpow.time), length(Data_logpow.time));
    meanperstim_low = zeros(num_low,length(Data_logpow.time), length(Data_logpow.time));

    high = 1:num_high;
    medium = num_high+1:num_high+num_med;
    low = num_high+num_med+1:num_high+num_med+num_low;

    rems = {high,medium,low};
    for rem = 1:length(rems)

        if rem == 1
            remname = 'high';
        elseif rem == 2
            remname = 'medium';
        elseif rem == 3
            remname = 'low';
        end

        for i1 = rems{rem}  % correlate all stimuli...*

            fprintf('RSA (sample %d of %d) of stimulus %d of %d with all other %d stimuli in %s condition (channel %d, subj %d) \n ' , sample_index, sample_num, i1, num_stims ,length(rems{rem})-1, remname, chan, actualsubj);
            % tf-data from first stimulus, i.e. 26x470         
            tf1 = squeeze(Data_logpow.powspctrm(i1,chan,:,:));
            % preallocate
            all_curr_corrmat = zeros(length(Data_logpow.time),length(Data_logpow.time));

            for i2 = rems{rem} % *...with all stimuli (but not with itself)
                if i1 ~= i2
                    % tf-data from second stimulus, 26x470
                    tf2 = squeeze(Data_logpow.powspctrm(i2,chan,:,:));
                    % calc RSA
                    % correlates tf1 and tf2, i.e. two stimuli -> 470x470 
                    % save memory and just add up the matrices
                    all_curr_corrmat = all_curr_corrmat + cm_spectral_rsa([], tf1, tf2, Data_logpow.time);
                end
            end

            % stack all means  (mean = sum/number of elements) (-1 because self-correlation (s1==s2) is not included)
            if rem == 1
                meanperstim_high(i1,:,:) = all_curr_corrmat/(length(rems{rem})-1);  
            elseif rem == 2
                meanperstim_med(i1-num_high,:,:) = all_curr_corrmat/(length(rems{rem})-1);  
            elseif rem == 3
                meanperstim_low(i1-(num_high+num_med),:,:) = all_curr_corrmat/(length(rems{rem})-1);                 
            end
        end
    end

    % stack for each sample
    GS.m.irem(sample_index,:,:) = mean(meanperstim_high); 
    GS.m.frem(sample_index,:,:) = mean(meanperstim_med);
    GS.m.nrem(sample_index,:,:) = mean(meanperstim_low);


    GS.info.dimord = 'time_time';
    GS.info.time = Data_logpow.time;
    GS.info.stim = Data_logpow.stim;
    GS.info.label = Data_logpow.label;
    GS.info.samples = sample_num;

    fprintf('sample %d of %d done \n' , sample_index, sample_num);              
    toc
end

% average samples
GS.m.irem = squeeze(mean(GS.m.irem,1));
GS.m.frem = squeeze(mean(GS.m.frem,1));
GS.m.nrem = squeeze(mean(GS.m.nrem,1));

% save  results
samplenum = [num2str(sample_num) '_samples',];
outname = [pn.eeg 'GS_' num2str(id) '_ch' num2str(chan) '_' samplenum];

save(outname, 'GS', '-v7.3');

end
