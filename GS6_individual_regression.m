% Runs individual regression analyses  

clear all
restoredefaultpath; clear RESTORE*

addpath('G:/ConMem/EEG/ConMemEEGTools/fieldtrip-20180709')
addpath('G:/ConMem/EEG/ConMemEEGTools/fnct_common')

%cd('/Volumes/FB-LIP/ConMem/EEG/MERLIN_ALL/G_RSA/A_GA')
cd('G:/ConMem/EEG/MERLIN_ALL/G_RSA/A_GA')

group = 'YA'

load([group '_GAGS_half_11.mat'],'GA11_h')
load([group '_GAGS_half_01.mat'],'GA01_h')
load([group '_GAGS_half_00.mat'],'GA00_h')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% settings for stat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cfg.channel          = 'all';
cfg.latency          = 'all';
cfg.parameter        = 'm';
cfg.frequency        = 'all';
cfg.avgoverchan      = 'no';
cfg.avgovertime      = 'no';
cfg.avgoverfreq      = 'no';
cfg.method           = 'analytic';
cfg.statistic        = 'indepsamplesregrT';

cfg.design = [1,2,3]; % high, medium, low
cfg.ivar        = 1;
cfg.cvar        = [];

dat1 = GA11_h;
dat1 = rmfield(dat1,'m');
dat2 = dat1; dat3 = dat1;

for subj = 1:size(GA11_h.m,1)
    
    subj

    dat1.m = GA11_h.m(subj,:,:,:);
    dat2.m = GA01_h.m(subj,:,:,:);
    dat3.m = GA00_h.m(subj,:,:,:);

    stat = ft_freqstatistics(cfg,dat3,dat2,dat1);
    stat.zval = cm_t2z(stat.stat,stat.df); % convert t to z
    
    if subj == 1
        GA_stat = stat;
        GA_stat = rmfield(GA_stat,{'stat','critval','prob','mask'});
        GA_stat.dimord = 'subj_chan_freq_time';
        GA_stat.tval = stat.stat;
    else
        GA_stat.zval = cat(4,stat.zval,GA_stat.zval);
        GA_stat.tval = cat(4,stat.stat,GA_stat.tval);
    end
    
    clear stat
end

% shift dimensions to usual ft format
GA_stat.zval = shiftdim(GA_stat.zval,3); 
GA_stat.tval = shiftdim(GA_stat.tval,3); 

save(['G:/ConMem/EEG/MERLIN_ALL/G_RSA/B_stat/' group '_ind_regr.mat'],'GA_stat','-v7.3')


