This repository provides Matlab code to reproduce the figures and run the main and additional analyses that are reported in ["Neural pattern similarity differentially relates to memory performance in younger and older adults"](https://www.jneurosci.org/content/39/41/8089) by Verena R. Sommer, Yana Fandakova, Thomas Grandy, Yee Lee Shing, Markus Werkle-Bergner, and Myriam C. Sander (2019) *Journal of Neuroscience* ([preprint](https://doi.org/10.1101/528620)).

Averaged data are provided on the corresponding project on the Open Science Framework [(OSF)](https://osf.io/p7v3s/).

Contact: [Verena R. Sommer (vsommer@mpib-berlin.mpg.de)][2]



## About the main analysis ##

The main analysis includes the between-item pattern similarity of EEG time-frequency data at each electrode and for each subject (`GS1_GlobalSimilarity.m`). These analyses were run on a high-performance computer cluster. The resulting individual channel x time x time similarity data for each memory quality are provided. For example, `YA_GAGS_half_11.mat` contains the similarity matrices of the high memory quality items for all young adults (YA). `11` denotes high, `01` medium, and `00` low memory quality. 

## Code ##

The provided scripts include analysis of:
 1. Global EEG pattern similarity: `GS...m` 
 2. Behavior, i.e. memory performance, strategy use, imagery rating: `Beh...m` 

and all custom and third-party dependencies including (modified) raincloud plots (by Micah Allen, Davide Poggiali, Kirstie Whitaker, Tom Rhys Marshall and Rogier Kievit; https://github.com/RainCloudPlots/RainCloudPlots) and Colorbrewer (by Cynthia Brewer; https://www.mathworks.com/matlabcentral/fileexchange/34087-cbrewer-colorbrewer-schemes-for-matlab), except the Fieldrip Toolbox which is available under http://www.fieldtriptoolbox.org/download/. 

## Data ##

The provided data can be found on [OSF](https://osf.io/p7v3s/) and include:
 1. Individual global similarity matrices (subject x channel x time x time, whereby one time-dimension is labeled as frequency in order to use standard Fieldtrip functions): `YA_GAGS...mat` and `OA_GAGS...mat`
 2. Averaged individual memory performance, i.e. number of items per memory quality: `BehDataMerlinAll.mat`
 3. Output from cluster-based random permutation statistics: e.g., `YA_11_vs_00.mat`

## Plots ##

To reproduce the plots presented in the paper, run:
 1. Figure 4: `Beh1_memory_performance.m`
 2. Figure 5A and B: `GS4b_plot_global_similarity.m`
 3. Figure 5C and D: ` GS5c_cluster_plot.m`
 4. Figure 5E: `GS7_individual_regr_z_cluster.m`
 


  [1]: https://gitlab.com/verysummer/merlin.-age-differences-in-neural-similarity-and-memory
  [2]: https://www.mpib-berlin.mpg.de/en/staff/verena-r-sommer