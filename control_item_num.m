function [ mean_cl ] = control_item_num( mean_cl )
%Additional Analysis
% exclude subjects outside range of overlapping item numbers in RSA 

% load lists (strange subj order because ids from study1 and study
% concatenated, same in GS data)
load('item_numbers_in_rsa.mat')

% overlapping range
min_all = max(min(listya_all(:,2)),min(listoa_all(:,2)));
max_all = min(max(listya_all(:,2)),max(listoa_all(:,2)));

too_small = find(listoa_all(:,2) < min_all);
too_large = find(listya_all(:,2) > max_all);

% exclude from data
mean_cl.OA(too_small,:) = [];
mean_cl.YA(too_large,:) = [];

end