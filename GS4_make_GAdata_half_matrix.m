% change GA GS data to half matrix only (incl. diagonal) & save

pn.root = 'G:/ConMem/EEG/';
merlin = 'MERLIN_ALL'; 
agegroups = {'YA','OA'}; 
rems = {'11','01','00'}; % memory qualities

for groupnr = 1:length(agegroups)    
    curr_group = agegroups{groupnr}; 
    
    for rem = 1:length(rems)
        curr_rem = rems{rem};
    
        load([pn.root merlin '/G_RSA/A_GA/' curr_group '_GAGS_' curr_rem '.mat'])
        
        % only upper triangle, incl diagonal
        GA_h = GA;
        GA_h = rmfield(GA_h,'m');
        for subj = 1:size(GA.m,1)
            disp(subj)
            for chan = 1:size(GA.m,2)
                GA_h.m(subj,chan,:,:) = triu(squeeze(GA.m(subj,chan,:,:)));  % upper triangle   
            end
        end
        GA_h.m(GA_h.m == 0) = NaN;
        clear GA
        
        % rename
        if strcmp(curr_rem,'11')
            GA11_h = GA_h;
            GA = 'GA11_h';
        elseif strcmp(curr_rem,'01')
            GA01_h = GA_h;
            GA = 'GA01_h';
        elseif strcmp(curr_rem,'00')
            GA00_h = GA_h;
            GA = 'GA00_h';
        end
        
        save([pn.root merlin '/G_RSA/A_GA/' curr_group '_GAGS_half_' curr_rem '.mat'],GA, '-v7.3')
        clear GA11_h
        
    end
end


