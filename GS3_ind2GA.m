% compute grand average file (keep individual)

% path definitions
pn.in = 'G:/ConMem/EEG/MERLIN/D_data/'; % where ind. data are
pn.out = 'G:/ConMem/EEG/MERLIN_ALL/G_RSA/A_GA/'; % where GA data will be saved

% which group?
subjects_wanted = 'YA'; % 'YA', 'OA', 'all'

% all IDs
ID = [3101;3102;3103;3104;3105;3106;3107;3108;3110;3111;3112;3113;3114;3115;3116;3118;3119;3122;3123;3125;3129;3131;3136;3137;3138;3139;3140;3201;3202;3203;3204;3205;3206;3207;3208;3209;3210;3211;3213;3214;3215;3218;3219;3220;3221;3222;3223;3224;3225;3230;4101;4102;4103;4104;4105;4106;4107;4108;4109;4110;4111;4112;4114;4115;4116;4117;4118;4119;4120;4121;4122;4123;4130;4133;4137;4138;4139;4140;4141;4144;4145;4146;4147;4201;4202;4203;4204;4205;4206;4207;4208;4209;4210;4211;4212;4213;4214;4215;4218;4220;4221;4222;4223;4225;4226;4227;4232;4240;4245;4247;4248;4249;4252];

if strcmp(subjects_wanted, 'YA')
    ID = ID(ID<4000);
elseif strcmp(subjects_wanted, 'OA')
    ID = ID(ID>4000);
end

%load some sample data 
load([pn.in  '3101/eeg/3101_L1_tfr_11_2_100Hz_ST.mat']) % tfrdat11 
tfrdat11.powspctrm = []; 


%% make GA

% memory quality categories
rems = {'11','01','00'};

for rem = 1:length(rems)
    curr_rem = rems{rem};

    for subjnr = 1:length(ID)    

        curr_subj = num2str(ID(subjnr));           
        display(['processing ID ' curr_subj])

        GS = load([pn.in curr_subj '/eeg/' curr_subj '_GS_' curr_rem '_allchan.mat']);

        %% get FT format for data
        fieldname = ['mean_GS_' curr_rem];
        GS.(fieldname).dimord = 'chan_freq_time'; % 
        GS.(fieldname).time = tfrdat11.time(32:end); % reduce bc of NaNs
        GS.(fieldname).freq = GS.(fieldname).time; % fake "freq" same as time
        GS.(fieldname).label = GS.(fieldname).info.label;
        gravr_GS{subjnr} = GS.(fieldname); % all subject data       
        clear GS
    end

    % settings for grand average
    cfg = [];
    cfg.keepindividual = 'yes'; 
    cfg.foilim         = 'all';
    cfg.toilim         = 'all';
    cfg.channel        = 'all';
    cfg.parameter      = 'm';

    GA = ft_freqgrandaverage(cfg,gravr_GS{:});   

    matsave = [pn.out subjects_wanted '_GAGS_' curr_rem '.mat'];
    save(matsave,'GA','-v7.3'); clear GA;
end

