function GS2_collect_all_channels(id,sample_num)
%Collects all RSA channel data in one file
%   input:  subj id string, e.g. '3101'
%           sample_num string, e.g. '20'
%   no output, but saves data with all channels in specified location

mypath = 'G:/ConMem/EEG/MERLIN/D_data/'; % adjust

% preallocate
mean_GS_11.m = zeros(60,470,470); % channel x time x time
mean_GS_01.m = zeros(60,470,470);     
mean_GS_00.m = zeros(60,470,470);

% collect data from all channles
for chan = 1:60

    % load data
    load([mypath id '/eeg/GS_' id '_ch' num2str(chan) '_' sample_num '_samples.mat']) 

    % save in new variable
    mean_GS_11.m(chan,:,:) = GS.m.irem;
    mean_GS_01.m(chan,:,:) = GS.m.frem;
    mean_GS_00.m(chan,:,:) = GS.m.nrem;
end

% save info 
mean_GS_11.info = GS.info;
mean_GS_01.info = GS.info;
mean_GS_00.info = GS.info;

save([mypath id '/eeg/' id '_GS_11_allchan'], 'mean_GS_11', '-v7.3')
save([mypath id '/eeg/' id '_GS_01_allchan'], 'mean_GS_01', '-v7.3')
save([mypath id '/eeg/' id '_GS_00_allchan'], 'mean_GS_00', '-v7.3')

end


