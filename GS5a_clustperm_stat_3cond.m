% runs cluster-based permutation test, regression t-statistic of all 3
% memory qualities

clear all
restoredefaultpath; clear RESTORE*

% path definitions
pn.root = 'G:/ConMem/EEG/';
merlin = 'MERLIN_ALL'; 

% EEG tools folder
pn.fun = [pn.root 'ConMemEEGTools/'];
addpath([pn.fun 'fieldtrip-20150930'])
ft_defaults

group = 'YA';

load([pn.root merlin '/G_RSA/A_GA/' group '_GAGS_half_11.mat'])
load([pn.root merlin '/G_RSA/A_GA/' group '_GAGS_half_01.mat'])
load([pn.root merlin '/G_RSA/A_GA/' group '_GAGS_half_00.mat'])

%load electrode layouts
load ('realistic_1005.mat');
cfg = [];
tmp.elec_sel = cm_elec2dataelec_20140226(realistic_1005, GA11_h);
cfg.elec    = tmp.elec_sel;
% correct some flipping of the elec
cfg.elec.pnt(:,1) = -tmp.elec_sel.pnt(:,2);
cfg.elec.pnt(:,2) = tmp.elec_sel.pnt(:,1);
clear tmp;
cfg.layout = ft_prepare_layout(cfg);   % error in R2017a


% settings for stat
cfg.latency          = 'all'; 
cfg.channel          = 'all'; 
cfg.frequency        = 'all'; 
cfg.avgovertime      = 'no'; 
cfg.avgoverchan      = 'no'; 
cfg.avgoverfreq      = 'no'; 
cfg.method           = 'montecarlo';
cfg.statistic        = 'depsamplesregrT';
cfg.correctm         = 'cluster';
cfg.clusteralpha     = 0.05; 
cfg.clusterstatistic = 'maxsum';
cfg.minnbchan        = 2; 
cfg.tail             = 0; 
cfg.clustertail      = 0; 
cfg.alpha            = 0.025; 
cfg.numrandomization = 1000;      
cfg.parameter        = 'm';

% specifies with which sensors other sensors can form clusters
cfg_neighb.method    = 'distance';
cfg.neighbours = ft_prepare_neighbours(cfg_neighb, cfg.elec);  

% design matrix
subj = size(GA11_h.m,1);
design(1,1:subj) = 1:subj; design(1,subj+1:2*subj)= 1:subj; design(1,2*subj+1:3*subj)= 1:subj; 
design(2,1:subj) = 1; design(2,subj+1:2*subj)= 2; design(2,2*subj+1:3*subj)= 3; 

cfg.design = design; 
cfg.ivar = 2; 
cfg.uvar = 1; 

% run 
stat = ft_freqstatistics(cfg, GA00_h, GA01_h, GA11_h);

matsave = [pn.root merlin '\G_RSA\B_stat\' group '_regr_3cond.mat'];
save(matsave,'stat'); clear stat;
