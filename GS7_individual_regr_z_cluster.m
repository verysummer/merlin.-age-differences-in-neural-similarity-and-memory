% Extracts z/t values (from GS6) in ya & oa clusters
% (from GS5a) and calculates t-tests
% Creates Figure 5E (raincloud plots)

clear all
restoredefaultpath; clear RESTORE*

pn.root = 'G:/ConMem/EEG/MERLIN_ALL/G_RSA';

groups = {'YA','OA'};

% want z or t value?
value = 'z'; 
control = true; % additional analysis excluding subjects with 'extreme' item numbers used in RSA

%% load z/t stats (subj x chan x time x time) (from GS6)

load([pn.root '/B_stat/YA_ind_regr.mat'])
if strcmp(value,'z')
    val.YA = GA_stat.zval;
elseif strcmp(value,'t')
    val.YA = GA_stat.tval;
end
clear GA_stat

load([pn.root '/B_stat/OA_ind_regr.mat'])
if strcmp(value,'z')
    val.OA = GA_stat.zval;
elseif strcmp(value,'t')
    val.OA = GA_stat.tval;
end
clear GA_stat


%% load clusters (from GS5a)

% ya-cluster mask
load([pn.root '/B_stat/YA_regr_3cond.mat'])
masky = NaN(size(stat.negclusterslabelmat));     
masky((find(stat.negclusterslabelmat ==1)))=1; % YA -> first neg cluster 

% oa-cluster mask
load([pn.root '/B_stat/OA_regr_3cond.mat'])
masko = NaN(size(stat.posclusterslabelmat));    
masko((find(stat.posclusterslabelmat ==1)))=1; % OA -> first pos cluster 


%% extract data from clusters

for g = 1:length(groups) % both groups
    
    group = groups{g}
    
    for m = 1:2 % both masks
        
        if m == 1
            mask = masky; % ya
        else
            mask = masko; % oa
        end
            
        for s = 1:size(val.(group),1) % all subjects      
            % exctract data within mask
            cl_dat(s,:,:,:) = squeeze(val.(group)(s,:,:,:)) .* mask;
        end
        
        % average across channels
        cl_dat = squeeze(nanmean(cl_dat,2));
        % average across t1
        cl_dat = squeeze(nanmean(cl_dat,3));       
        % average across t2, save for both groups and masks
        mean_cl.(group)(:,m) = squeeze(nanmean(cl_dat,2));
        
        clear cl_dat 
    end
end

save([pn.root '/B_stat/YA+OA_ind_' value '_from_clusters.mat'],'mean_cl','-v7.3')


%% control for different item numbers for ya and oa by
% excluding subjects outside range of overlapping item numbers in RSA

if control
    mean_cl = control_item_num(mean_cl);
end

%% test diff from 0

% which cluster
cl_y = 1;
cl_o = 2;

[H,P,CI,STATS] = ttest(mean_cl.YA(:,cl_y))
[H,P,CI,STATS] = ttest(mean_cl.YA(:,cl_o))
[H,P,CI,STATS] = ttest(mean_cl.OA(:,cl_o))
[H,P,CI,STATS] = ttest(mean_cl.OA(:,cl_y))


%% figure ya & oa in ya/oa cluster

bw = .06 % .06 (z), .5 (t)

figure
for cl = 1:2
    
    if cl == 1
        clg = 'ya-';
    else
        clg = 'oa-';
    end

    subplot(2,1,cl)
    h1 = raincloud_plot_vrs('X', mean_cl.OA(:,cl), 'box_on', 1,'bandwidth', bw, 'color', [163/255 38/255 62/255], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .55, 'dot_dodge_amount', .75, 'box_col_match', 1);%     
    h1 = raincloud_plot_vrs('X', mean_cl.YA(:,cl), 'box_on', 1,'bandwidth', bw, 'color', [0 108/255 175/255], 'alpha', 0.5, 'box_dodge', 1, 'box_dodge_amount', .15, 'dot_dodge_amount', .35, 'box_col_match', 1); %   
    set(gca,'ytick',[],'FontSize',16)
    ax=gca;
    ax.XTickLabelRotation = 270;

    [h,p,ci,stat] = ttest2(mean_cl.YA(:,cl),mean_cl.OA(:,cl))
end

if control
    print(['raincloud_' value '_in_clusters_only_overl_stimnum'],'-dpdf')
else
    print(['raincloud_' value '_in_clusters'],'-dpdf')
end


