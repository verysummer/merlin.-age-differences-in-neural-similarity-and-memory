% Calculates means and makes figures of global similarity grand average (GAGS) data: 
% similarity matrices and diagonals of high, medium, low memory quality
% Figure 5A + B

close all
clear all 
pack 

groups = {'YA','OA'};

%% load grand averages 

% load 
for g = 1:length(groups)
    curr_group = groups{g};
    load([curr_group '_GAGS_half_11.mat'])
    GA_irem.(curr_group) = GA11_h;
    clear GA11_h
    load([curr_group '_GAGS_half_01.mat'])
    GA_frem.(curr_group) = GA01_h;
    clear GA01_h
    load([curr_group '_GAGS_half_00.mat'])
    GA_nrem.(curr_group) = GA00_h;
    clear GA00_h
    subjno.(curr_group) = size(GA_irem.(curr_group).m,1);
end

% average 
for g = 1:length(groups)
    curr_group = groups{g};
    % across channels
    mean_GA_irem.(curr_group) = squeeze(mean(GA_irem.(curr_group).m,2));
    mean_GA_frem.(curr_group) = squeeze(mean(GA_frem.(curr_group).m,2));
    mean_GA_nrem.(curr_group) = squeeze(mean(GA_nrem.(curr_group).m,2));

    % across subjects
    mean_GA_irem.(curr_group) = squeeze(mean(mean_GA_irem.(curr_group),1));
    mean_GA_frem.(curr_group) = squeeze(mean(mean_GA_frem.(curr_group),1));
    mean_GA_nrem.(curr_group) = squeeze(mean(mean_GA_nrem.(curr_group),1));
end


%% similarity matrices (Figure 5A)

% create colormap
Cm = cbrewer('seq', 'YlOrRd', 64); 

time = GA_irem.YA.time;

for g = 1:length(groups)
    curr_group = groups{g};

    maxi = max([max(max(mean_GA_irem.(curr_group))), max(max(mean_GA_frem.(curr_group))), max(max(mean_GA_nrem.(curr_group)))]);
    mini = min([min(min(mean_GA_irem.(curr_group))), min(min(mean_GA_frem.(curr_group))), min(min(mean_GA_nrem.(curr_group)))]);

    if g == 1
        num = 1;
        figure%('Position', [200 600 2000 700])
        colormap(Cm)
    else
        num = 4;
    end

    subplot(2,3,num)
    i=imagesc(time,time,mean_GA_irem.(curr_group), [mini maxi]);
    set(gca,'Ydir','normal')
    axis square
    c=colorbar;
    c.FontSize = 14;
    c.Label.String = 'z''';
    c.Label.FontSize = 14;
    title('strong','FontSize',16)
    xlabel('trial time','FontSize',18)           
    ylabel('trial time','FontSize',18)            
    ax = gca;
    ax.FontSize = 14;
    set(i,'AlphaData',~isnan(mean_GA_irem.(curr_group))) % makes nans white

    subplot(2,3,num+1)
    i=imagesc(time,time,mean_GA_frem.(curr_group), [mini maxi]);
    set(gca,'Ydir','normal')
    axis square
    c=colorbar;
    c.FontSize = 14;
    c.Label.String = 'z''';
    c.Label.FontSize = 14;
    title('weak','FontSize',16)
    xlabel('trial time','FontSize',18)            
    ylabel('trial time','FontSize',18)     
    ax = gca;
    ax.FontSize = 14;
    set(i,'AlphaData',~isnan(mean_GA_irem.(curr_group))) % makes nans white

    subplot(2,3,num+2)
    i=imagesc(time,time,mean_GA_nrem.(curr_group), [mini maxi]);
    set(gca,'Ydir','normal')
    axis square
    c=colorbar;
    c.FontSize = 14;
    c.Label.String = 'z''';
    c.Label.FontSize = 14;
    title('no','FontSize',16)
    xlabel('trial time','FontSize',18)            
    ylabel('trial time','FontSize',18)    
    ax = gca;
    ax.FontSize = 14;
    set(i,'AlphaData',~isnan(mean_GA_irem.(curr_group))) % makes nans white
end
    

%% plot diagonals of similarity matrices (Figure 5B)

figure
for g = 1:length(groups)
    curr_group = groups{g};

    irem.(curr_group) = diag(mean_GA_irem.(curr_group));
    frem.(curr_group) = diag(mean_GA_frem.(curr_group));
    nrem.(curr_group) = diag(mean_GA_nrem.(curr_group));

    plot(time, frem.(curr_group), 'Color',[77/255,190/255,238/255],'LineWidth', 1.5)
    hold on
    plot(time, nrem.(curr_group), 'Color',[192/255,192/255,192/255],'LineWidth', 1.5)
    hold on
    plot(time, irem.(curr_group), 'Color',[20/255,43/255,140/255],'LineWidth', 1.5)
    l=legend('medium','low','high');
    xlabel('trial time','FontSize',16)            
    ylabel('similarity (z'')','FontSize',16)  
    ax = gca;
    ax.FontSize = 14;
end

