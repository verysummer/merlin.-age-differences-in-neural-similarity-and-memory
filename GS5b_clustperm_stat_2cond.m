% runs cluster-based permutation test, dependent samples t-statistic 
% of 2 specified memory qualities 

clear all
restoredefaultpath; clear RESTORE*

% set:
group = 'YA';
cond1 = '11';
cond2 = '00';

% path definitions
pn.root = 'G:/ConMem/EEG/';
merlin = 'MERLIN_ALL'; 

% EEG tools folder
pn.fun = [pn.root 'ConMemEEGTools/'];
addpath([pn.fun 'fieldtrip-20150930'])
ft_defaults

load([pn.root merlin '/G_RSA/A_GA/' group '_GAGS_half_' cond1 '.mat'])
load([pn.root merlin '/G_RSA/A_GA/' group '_GAGS_half_' cond2 '.mat'])

dat1 = eval(['GA' cond1 '_h']);
dat2 = eval(['GA' cond2 '_h']);

%load electrode layouts
load ('realistic_1005.mat');
cfg = [];
tmp.elec_sel = cm_elec2dataelec_20140226(realistic_1005, dat1);
cfg.elec    = tmp.elec_sel;
% correct some flipping of the elec
cfg.elec.pnt(:,1) = -tmp.elec_sel.pnt(:,2);
cfg.elec.pnt(:,2) = tmp.elec_sel.pnt(:,1);
clear tmp;
cfg.layout = ft_prepare_layout(cfg);   % error in R2017a


% settings for stat
cfg.latency          = 'all'; 
cfg.channel          = 'all'; 
cfg.frequency        = 'all'; 
cfg.avgovertime      = 'no'; 
cfg.avgoverchan      = 'no'; 
cfg.avgoverfreq      = 'no'; 
cfg.method           = 'montecarlo';
cfg.statistic        = 'depsamplesT';
cfg.correctm         = 'cluster';
cfg.clusteralpha     = 0.05; 
cfg.clusterstatistic = 'maxsum';
cfg.minnbchan        = 2; 
cfg.tail             = 0; 
cfg.clustertail      = 0; 
cfg.alpha            = 0.025; 
cfg.numrandomization = 1000;      
cfg.parameter        = 'm';

% specifies with which sensors other sensors can form clusters
cfg_neighb.method    = 'distance';
cfg.neighbours = ft_prepare_neighbours(cfg_neighb, cfg.elec);  

% design matrix
subj = size(dat1.m,1);
design(1,1:subj) = 1:subj; design(1,subj+1:2*subj)= 1:subj;  
design(2,1:subj) = 1; design(2,subj+1:2*subj)= 2; 

cfg.design = design; 
cfg.ivar = 2; 
cfg.uvar = 1; 

% run 
stat = ft_freqstatistics(cfg, dat1, dat2);

matsave = [pn.root merlin '\G_RSA\B_stat\' group '_' cond1 '_vs_' cond2 '.mat'];
save(matsave,'stat'); clear stat;




